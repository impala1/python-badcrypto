BadCrypto
=========

About
-----

Python implementation of some cryptographic algorithms for fun, not profit. As one is typically not supposed to roll one's own crypto, the repo's name reflects this. That said, everything is (or should be) unit tested.


Currently Implemented
---------------------

* modular arithmetic and other asymetric cryptography primitives (partially)


TODO
----

* [Shamir's secret sharing](https://en.wikipedia.org/wiki/Shamir%27s_secret_sharing)
* RSA
* [Wiener's RSA attack method](https://en.wikipedia.org/wiki/Wiener%27s_Attack)
* [ElGamal encryption](https://en.wikipedia.org/wiki/ElGamal_encryption)
* ???


Usage
-----

Did you read the first paragraph? *This isn't supposed to be secure!*
