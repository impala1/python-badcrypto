from ..primitives import *
import unittest
import operator

class TestPrimitiveFunctions(unittest.TestCase):
    def setUp(self):
        self.a = 1814274
        self.b = 259896
        self.prime_a = 195845982777569926302400511L
        self.prime_b = 4776913109852041418248056622882488319L
        self.s = 'foo'


    def test_extended_gcd_2arg(self):
        x, y = extended_gcd_2arg(self.a, self.b)
        self.assertEqual(self.a * x + self.b * y, 4998)

        # prime numbers have no common factors
        x, y = extended_gcd_2arg(self.prime_a, self.prime_b)
        self.assertEqual(self.prime_a * x + self.prime_b * y, 1)


    def test_inverse_mod_n(self):
        ans = inverse_mod_n(self.prime_b, self.prime_a)
        self.assertEqual(ans, 21491509174293277846405666L)

        # numbers that aren't coprime with the modulus cannot be inversed
        self.assertRaises(AssertionError, inverse_mod_n, self.a, self.b)


    def test_bytearray2long(self):
        ans = bytearray2long(bytearray(self.s))
        real_ans = ord(self.s[2]) + ord(self.s[1])*256 + ord(self.s[0])*256*256
        self.assertEqual(ans, real_ans)
        self.assertRaises(ValueError, bytearray2long, bytearray())


    def test_long2bytearray(self):
        self.assertEqual(bytearray(0), long2bytearray(0))
        a, b, c = 5, 7, 9
        self.assertEqual(bytearray((a, b, c)), long2bytearray(a*256*256+b*256+c))


    def test_div_modulo(self):
        self.assertEqual((1,), tuple(div_modulo(1, 1, 8)))
        self.assertEqual((0, 4), tuple(div_modulo(1, 2, 8)))
        # when the numbers are coprime, there is only one solution
        self.assertEqual(1, len(list(div_modulo(self.prime_a, 2, self.prime_b))))


    def test_div_uninvertable(self):
        ans = div_uninvertable(1, 2, 8, lambda n: n**2 == 16)
        self.assertEqual(4, ans)
        self.assertRaises(ValueError, div_uninvertable, 1, 2, 8, lambda _: False)


class TestModulusInit(unittest.TestCase):
    def setUp(self):
        self.a = ModulusInt(6, 2796203)
        self.b = ModulusInt(9, 174763)
        self.c = ModulusInt(7, 11)


    def test_init(self):
        self.assertEqual(5, ModulusInt(5, 11))
        self.assertEqual(1, ModulusInt(12, 11))
        self.assertRaises(ValueError, ModulusInt, 1, 0)


    def test_add(self):
        self.assertEqual(0, self.c + 4)
        self.assertRaises(ValueError, operator.add, self.c, self.a)
        self.assertEqual(10, self.b + 1)
        self.assertEqual(8, self.b + -1)
        self.assertEqual(self.b, self.b + self.b._limit)
        self.assertEqual(self.b, self.b._limit + self.b)
        self.assertEqual(self.b, self.b + 0)
        self.assertEqual(self.b, 0 + self.b)


    def test_sub(self):
        self.assertEqual(0, self.c - self.c)
        self.assertEqual(self.a, self.a - 0)
        self.assertEqual(self.c + 999, self.c - (-999))
        self.assertRaises(ValueError, operator.sub, self.a, self.b)
        self.assertEqual(self.b, self.b - self.b._limit)


    def test_mul(self):
        self.assertEqual(0, self.a * 0)
        self.assertEqual(0, 0 * self.a)
        self.assertEqual(self.b, self.b * 1)
        self.assertEqual(self.b, 1 * self.b)
        self.assertEqual(2, self.c * 5)
        self.assertEqual(4, self.c * -1)
        self.assertEqual(4, -1 * self.c)


    def test_div(self):
        self.assertEqual(self.c, self.c / 1)
        self.assertRaises(ZeroDivisionError, operator.div, self.c, 0) # not sure if correct?
        self.assertEqual(1, self.c / -4)
        self.assertRaises(ValueError, operator.div, self.c, 11)


    def test_pow(self):
        self.assertEqual(646733, self.a**123456)
        self.assertEqual(1, self.b**0)
        self.assertEqual(466034, self.a**-1)


    def test_rdiv(self):
        self.assertRaises(ZeroDivisionError, operator.div, 1, ModulusInt(0, 11))
        self.assertEquals(2, 18 / self.b)
        self.assertEquals(0, 0 / self.c)


    def test_neg(self):
        self.assertEquals(4, -self.c)
        self.assertEquals(0, ModulusInt(0, 1))
