def extended_gcd_2arg(a, b):
    """Compute the GCD of two numbers."""
    sgn = lambda x: cmp(x, 0)

    sa, sb = sgn(a), sgn(b)
    a, b = abs(a), abs(b)
    x, lastx = 0, 1
    y, lasty = 1, 0

    while b != 0:
        q = a / b
        a,b = b, a % b
        x,lastx = lastx - q * x, x
        y,lasty = lasty - q * y, y

    return (lastx * sa, lasty * sb)


def inverse_mod_n(a, n):
    """Calculate the inverse mod of a number."""
    x, y = extended_gcd_2arg(a, n)
    assert x * a + y * n == 1, x * a + y * n
    return x


def bytearray2long(ba):
    """Convert bytearray to a scalar number."""
    if len(ba) == 0:
        raise ValueError

    out = 0L
    for byte in ba:
        out = out << 8
        out += byte
    return out


def long2bytearray(num):
    """Convert a number into raw bytes."""
    out = bytearray()
    while num:
        tmp = num & 255
        out.append(tmp)
        num = num >> 8
    out.reverse()
    return out


def div_modulo(x, y, n):
    """Perform modular division on a number.

    This function returns an iterable of possible candidates,
    which happens when the divisor and the modulus are not coprime.
    """
    a,b = extended_gcd_2arg(y, n)
    g = a*y + b*n
    return ((a*x/g + i*n/g) % n for i in xrange(g))


def div_uninvertable(a, b, p, check):
    """Pefrom modular division on a number, when it is expected
    that there are multiple answers.

    This function is similar to div_modulo(), but provides another argument
    for a callable to check each of the possible answers mathematically
    to find the correct one.

    For example, if you have a number that is a product of one of the solutions
    and another number, you can pass that expression into this function as
    a lambda to check each possible answer for correctness.
    """
    for i in div_modulo(long(a), long(b), p):
        if check(i):
            return i
    raise ValueError('These values are not divisible: %s / %s mod %s' % (a, b, p))


class ModulusInt(long):
    """Class for performing modular arithmetic as you would with regular integers."""
    def __init__(self, num, limit):
        """Create a new modular integer.

        'num' can be any integer type, or a bytearray if dealing with
        binary data.

        'limit' or modulus, is the number at which you want the numbers
        to wrap around at.
        """
        self._limit = limit

    def __new__(cls, num, limit, *args, **kwargs):
        """Called before the constructor, overrides the __new__() in long.

        This is required as the numeric value is immutable once set.
        """
        if limit < 1:
            raise ValueError('Modulus may not be less than 1.')

        if isinstance(num, bytearray):
            num = bytearray2long(num)

        return long.__new__(cls, num % limit, *args, **kwargs)


    def __add__(self, n):
        """Do modular addition.

        Creates a new instance and returns it.
        """
        if isinstance(n, self.__class__) and n._limit != self._limit:
            raise ValueError('Cannot add numbers with different limits. (%d vs %d).' \
                                % (n._limit, self._limit))

        if not isinstance(n, long) and not isinstance(n, int):
            raise ValueError('Cannot add anything other than integers with this type.')

        return self.__class__(n + long(self), self._limit)


    def __sub__(self, n):
        """Do modular subtraction.

        Creates a new instance and returns it.
        """
        return self + (-n)


    def __mul__(self, n):
        """Do modular multiplication.

        Creates a new instance and returns it.
        """
        if not isinstance(n, long) and not isinstance(n, int):
            raise ValueError('Cannot multiply by anything other than an integer.')

        return self.__class__(n * long(self), self._limit)


    def __div__(self, n):
        """Do modular division.

        Creates a new instance and returns it.
        """
        if n == 0:
            raise ZeroDivisionError

        try:
            return self * inverse_mod_n(n, self._limit)
        except AssertionError:
            raise ValueError('Cannot divide by this number: not coprime with the limit.')


    def __pow__(self, n):
        """Raise number to an exponent, using modular arithmetic.

        Creates a new instance and returns it.
        """
        if n < 0:
            return ModulusInt(inverse_mod_n(self, self._limit), self._limit) ** -n

        ans = pow(long(self), n, self._limit)
        return self.__class__(ans, self._limit)


    def __rdiv__(self, n):
        """Do modular division.

        Creates a new instance and returns it, assuming the argument is an
        instance of this class. Otherwise performs regular division.
        """
        return n / long(self)


    def __neg__(self):
        """Invert this number using modular arithmetic."""
        return self.__class__(-long(self), self._limit)


    def __repr__(self):
        """Display number as an easy to read string."""
        return '%d mod %d' % (self, self._limit)

    # alias the functions, same thing anyway
    __radd__ = __add__
    __rmul__ = __mul__
