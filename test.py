import unittest
from badcrypto.tests import primitives
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Execute unit tests.')
    parser.add_argument('-v', '--verbose', action="store_true")
    args = parser.parse_args()

    verbosity = 2 if args.verbose else 1

    suite = unittest.TestLoader().loadTestsFromModule(primitives)
    unittest.TextTestRunner(verbosity=verbosity).run(suite)
